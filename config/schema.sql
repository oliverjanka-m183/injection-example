USE `test` ;

CREATE TABLE IF NOT EXISTS `test`.`User` (
  `username` VARCHAR(64) NOT NULL,
  `hashedPassword` VARCHAR(255) NOT NULL,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE,
  PRIMARY KEY (`username`))
ENGINE = InnoDB;

INSERT INTO `test`.`User` (`username`, `hashedPassword`) VALUES ('admin', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4');