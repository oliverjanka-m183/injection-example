<?php 
require_once __DIR__ . "/main.php";

$search = isset($_REQUEST['q']) ? $_REQUEST['q'] : '';
// $search = isset($_REQUEST['q']) ? htmlspecialchars($_REQUEST['q']) : '';
?>
<html>
    <body>
        <a href="login.php">Login</a>
        <h1>Search</h1>
        <form>
            <input type="text" name="q" placeholder="Search" value="<?= $search ?>">
            <button type="submit">Search</button>
        </form>
        <?php 
        if (!empty($search)) {
            echo "<div>Searching for: $search</div>";
        }
        ?>
    </body>
</html>