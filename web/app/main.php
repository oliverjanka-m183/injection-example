<?php 
$mysqli = new mysqli(
    /*host*/        'mysql',
    /*username*/    $_SERVER['MYSQL_USER'],
    /*password*/    $_SERVER['MYSQL_PASSWORD'],
    /*database*/    $_SERVER['MYSQL_DATABASE']
);
if ($mysqli->connect_errno) {
    throw new ErrorException("Error while connecting to database: " . $this->mysqli->connect_error);
}
?>