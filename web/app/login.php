<?php
// Login without password: ' OR 1=1;-- 
require_once __DIR__ . "/main.php";

$loggedInUser = null;
$loginFailed = false;

function insecureLogin() {
    global $mysqli;
    global $loggedInUser;
    global $loginFailed;
    $reqUsername = $_REQUEST['username'];
    $reqPassword = hash('sha256', $_REQUEST['password']);

    $query = "SELECT * FROM User WHERE username='$reqUsername' AND hashedPassword='$reqPassword';";
    $result = $mysqli->query($query);
    if ($result->num_rows >= 1) {
        $loggedInUser = $reqUsername;
        return true;
    } else {
        $loginFailed = true;
        return false;
    }
}

function secureLogin() {
    global $mysqli;
    global $loggedInUser;
    global $loginFailed;
    $reqUsername = $_REQUEST['username'];
    $reqPassword = hash('sha256', $_REQUEST['password']);

    $stmt = $mysqli->prepare("SELECT * FROM User WHERE username=? AND hashedPassword=?;");
    $stmt->bind_param("ss", $reqUsername, $reqPassword);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows >= 1) {
        $loggedInUser = $reqUsername;
        return true;
    } else {
        $loginFailed = true;
        return false;
    }
}

$loginMethod = 'insecureLogin';
// $loginMethod = 'secureLogin';

if (isset($_REQUEST['username']) && isset($_REQUEST['password']) && $loginMethod()) { 
    
?>
<html>
    <body>
        Logged in as <?= $loggedInUser ?>. <a href="login.php">go back</a>
    </body>
</html>

<?php } else { ?>
<html>
    <body>
        <div><?= $loginFailed ? "Login failed" : "" ?></div>
        <form method="POST">
            <label for="username">Username</label>
            <input type="text" name="username" id="username" /><br>
            <label for="password">Password</label>
            <input type="password" name="password" id="password" /><br>
            <button type="submit">SUBMIT</button>
        </form>
    </body>
</html>
<?php }

?>